package strategy;

public class Main {
    public static void main(String[] args) {
        Context context = new Context(new DownloadWindows());
        context.download("file.txt");
        context = new Context(new DownloadLinux());
        context.download("file.txt");
    }
}
