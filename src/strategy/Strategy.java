package strategy;

public interface Strategy {
    void download(String file);
}
