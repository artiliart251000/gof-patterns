package factoryMethod;

public class NotificationFactory {
    public Notification createNotification(String channel){
        switch (channel){
            case "SMS":
                return new SmsNotification();
            case "EMAIL":
                return new EmailNotification();
            default:
                throw new IllegalArgumentException("Неизвестный способ" + channel);
        }
    }
}
