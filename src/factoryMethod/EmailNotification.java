package factoryMethod;

public class EmailNotification implements Notification{
    @Override
    public void notifyMessage() {
        System.out.println("Отправляю сообщение на почту");
    }
}
