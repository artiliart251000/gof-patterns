package factoryMethod;

public class SmsNotification implements Notification{
    @Override
    public void notifyMessage() {
        System.out.println("Отправляю сообщение на телефон");
    }
}
