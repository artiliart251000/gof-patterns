package factoryMethod;

public class Main {

    public static void main(String[] args) {
        NotificationFactory notificationFactory = new NotificationFactory();
        Notification sms = notificationFactory.createNotification("SMS");
        Notification email = notificationFactory.createNotification("EMAIL");
        sms.notifyMessage();
        email.notifyMessage();
    }
}
