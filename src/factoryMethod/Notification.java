package factoryMethod;

public interface Notification {
    void notifyMessage();
}
