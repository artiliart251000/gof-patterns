package adapter;

public class Compatibility implements USB{
    private Card card;

    public Compatibility(Card card){
        this.card = card;
    }

    @Override
    public void connectWithCable(){
        this.card.insert();
    }

}
