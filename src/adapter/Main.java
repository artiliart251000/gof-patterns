package adapter;

public class Main {
    public static void main(String[] args) {
        USB compatibility = new Compatibility(new Card());
        compatibility.connectWithCable();
    }

}
