package adapter;

public interface USB {
    void connectWithCable();
}
